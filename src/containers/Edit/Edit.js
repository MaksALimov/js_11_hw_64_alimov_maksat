import React, {useEffect, useState} from 'react';
import dayjs from "dayjs";
import axios from "axios";
import './Edit.css';
import Spinner from "../../components/Spinner/Spinner";

const Edit = ({match, history}) => {
    const [editPost, setEditPost] = useState({
        title: '',
        textArea: '',
        time: dayjs().format('DD. MM. YYYY. HH:mm'),
    });

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const oldData = async () => {
            const response = await axios.get('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json');
            setEditPost({
                title: response.data.addPost.title,
                textArea: response.data.addPost.textArea,
                time: response.data.addPost.time
            })
        }
        oldData().catch(e => console.log(e));
    }, [match.params.id]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setEditPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const redactPost = e => {
        e.preventDefault();

        setLoading(true);
        const fetchData = async () => {
            await axios.put('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json', {
                addPost: {
                    title: editPost.title,
                    textArea: editPost.textArea,
                    time: editPost.time,
                },
            });
            setLoading(false);
            history.replace('/');
        };
        fetchData().catch(e => console.log(e));
    };

    let form = (
        <form onSubmit={redactPost} className="FormWrapper">
            <label>
                <input
                    type="text"
                    name="title"
                    value={editPost.title}
                    placeholder="Title"
                    onChange={onInputChange}
                />
            </label>
            <textarea
                cols="50"
                rows="10"
                name="textArea"
                value={editPost.textArea}
                placeholder="Description"
                onChange={onInputChange}
            />
            <p>
                {editPost.time}
            </p>
            <button type="submit">
                Save
            </button>
        </form>
    )

    if (loading) {
        form = <Spinner/>
    }

    return (
        <>
            {form}
        </>
    );
};

export default Edit;