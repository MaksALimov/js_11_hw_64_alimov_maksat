import React, {useEffect, useState} from "react";
import axios from "axios";
import {NavLink} from "react-router-dom";
import './Home.css';

const Home = () => {
    const [posts, setPosts] = useState([]);
    const [id, setId] = useState([]);

    useEffect(() => {
        const postData = async () => {
            const response = await axios.get('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts/.json');
            setId(Object.keys(response.data));
            setPosts(Object.values(response.data));
        };

        postData().catch(e => console.log(e));
    }, []);

    return (
        <div>
            {posts.length > 0 ? posts.map((value, i) => {
                return (
                    <div key={i} className="Post">
                        <p className="PostTime">
                            Time: {value.addPost.time}
                        </p>
                        <h3 className="PostTitle">
                            Title: {value.addPost.title}
                        </h3>
                        <NavLink to={`/posts/${id[i]}`}>
                            <button className="ReadMore">Read more</button>
                        </NavLink>
                    </div>
                )
            }) : <p className="Message">Постов пока нет</p>}
        </div>
    );
};

export default Home;