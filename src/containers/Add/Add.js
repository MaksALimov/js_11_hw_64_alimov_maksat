import React, {useState} from 'react';
import dayjs from "dayjs";
import axios from "axios";
import './Add.css';

const Add = () => {
    const [addPost, setAddPost] = useState({
        title: '',
        textArea: '',
        time: dayjs().format('DD. MM. YYYY. HH:mm'),
    });

    const [userHelp, setUserHelp] = useState(false);

    const onInputChange = e => {
        const {name, value} = e.target;

        setAddPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const sendPost = async e => {
        e.preventDefault();

        try {
            await axios.post('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts.json', {
                addPost,
            });
        } catch (e) {
            console.log(e);
        }

        setAddPost({
            title: '',
            textArea: '',
        });

    };

    return (
        <form onSubmit={sendPost} className="formWrapper">
            <label>
                <input
                    type="text"
                    value={addPost.title}
                    name="title"
                    placeholder="title of post"
                    onChange={onInputChange}/>
            </label>
            <label>
                <textarea
                    cols="50"
                    rows="10"
                    name="textArea"
                    placeholder="description of post"
                    value={addPost.textArea}
                    onChange={onInputChange}
                />
            </label>
            <button type="submit" className="Save" onClick={() => setUserHelp(true)}>
                Save
            </button>
            {userHelp ? <p className="UserHelp">Перейдите на страницу Home что увидеть свой пост</p> : null}
        </form>
    );
};

export default Add;