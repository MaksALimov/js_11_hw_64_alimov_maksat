import React, {useEffect, useState} from 'react';
import axios from "axios";
import {NavLink} from "react-router-dom";
import Spinner from "../../components/Spinner/Spinner";

const FullPost = ({match, history}) => {
    const [fullPost, setFullPost] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const getFullPost = async () => {
            const response = await axios.get('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json');
            setFullPost(Object.values(response.data));
        };

        getFullPost().catch(e => console.log(e));
    }, [match.params.id]);

    const deletePost = () => {
        setLoading(true);
        const deletePost = async () => {
            await axios.delete('https://my-blog-lab-64-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json');
            setLoading(false);
            history.replace('/')
        };

        deletePost().catch(e => console.log(e));
    };

    let post = (
        fullPost.map((value, i) => (
            <div key={i} className="Post">
                <p className="PostTime">
                    Time: {value.time}
                </p>
                <p className="PostTitle">
                    Title: {value.title}
                </p>
                <p className="PostDescription">
                    Description : {value.textArea}
                </p>
                <button onClick={deletePost} className="DeleteBtn">
                    Delete
                </button>
                <NavLink to={`/posts/${match.params.id}/edit`}>
                    <button className="EditBtn">
                        Edit
                    </button>
                </NavLink>
            </div>
        ))
    );

    if (loading) {
        post = <Spinner/>
    }

    return (
        <div>
            {post}
        </div>
    );
};

export default FullPost;