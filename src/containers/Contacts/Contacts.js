import React, {useEffect, useState} from 'react';
import axios from "axios";

const Contacts = () => {
    const [contacts, setContacts] = useState([{
        contactsText: '',
    }]);

    const [input, setInput] = useState(false);
    const [editedInput, setEditedInput] = useState({editedText: '',});
    const [showSaveBtn, setShowSaveBtn] = useState(false);

    useEffect(() => {
        const contactsData = async () => {
          const response = await axios.get('https://my-blog-lab-64-default-rtdb.firebaseio.com/pages/contacts.json');
            setContacts([{contactsText: response.data}]);
        };

        contactsData().catch(console.log);
    }, []);

    const editAbout =  () => {
        setInput(true);
        setShowSaveBtn(true);
    };

    const saveEdit = async () => {
        const response = await axios.put('https://my-blog-lab-64-default-rtdb.firebaseio.com/pages/about.json', {
            contactsText: editedInput.editedText
        });
        setContacts([response.data])
        setInput(false)
        setShowSaveBtn(false);
    }


    return (
        <div className="AboutWrapper">
            {contacts.map((text, i) => (
                <p key={i} className="AboutText">{text.contactsText}</p>
            ))}
            <button onClick={editAbout} className="AboutEditBtn">Edit</button>
            {input ?
                <>
                    <textarea rows="10" cols="30" value={editedInput.editedText} onChange={e => setEditedInput({editedText: e.target.value})}/>
                    {showSaveBtn ? <button onClick={saveEdit} className="AboutSaveBtn">Save</button>: null}
                </>
                : null}
        </div>
    );
};

export default Contacts;