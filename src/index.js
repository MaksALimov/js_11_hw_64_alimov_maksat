import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import Add from "./containers/Add/Add";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import FullPost from "./containers/FullPost/FullPost";
import Edit from "./containers/Edit/Edit";

ReactDOM.render(
    <BrowserRouter>
        <div className="wrapper">
            <div>
                <h2 className="MyBlogTitle">My Blog</h2>
            </div>
            <nav>
                <ul className="MyBlogNav">
                    <NavLink to={'/'}>
                        <li>
                            Home
                        </li>
                    </NavLink>
                   <NavLink to={'/posts/add'}>
                       <li>
                           Add
                       </li>
                   </NavLink>
                    <NavLink to={'/about'}>
                        <li>
                            About
                        </li>
                    </NavLink>
                   <NavLink to={'/contacts'}>
                       <li>
                           Contacts
                       </li>
                   </NavLink>
                </ul>
            </nav>
        </div>
       <Switch>
           <Route path="/" exact component={App}/>
           <Route path="/posts" exact component={App}/>
           <Route path="/posts/add" component={Add}/>
           <Route path="/posts/:id" exact component={FullPost}/>
           <Route path="/posts/:id/edit" component={Edit}/>
           <Route path="/about" component={About}/>
           <Route path="/contacts" component={Contacts}/>
       </Switch>
    </BrowserRouter>,
  document.getElementById('root')
);
